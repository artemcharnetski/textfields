//
//  ViewController.swift
//  TextFields
//
//  Created by Артём Чернецкий on 26/04/2019.
//  Copyright © 2019 Артём Чернецкий. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var numberAndPunctuationTextField: CustomTextField!
    @IBOutlet weak var maxFiveCharactersTextField: CustomTextField!
    @IBOutlet weak var expiredDateTextField: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberAndPunctuationTextField.title = "Number and punctuation marks"
        numberAndPunctuationTextField.type = .numberAndPunctuationCharacters
        
        maxFiveCharactersTextField.title = "Max five characters"
        maxFiveCharactersTextField.type = .maxFiveCharacters
        
        expiredDateTextField.title = "Expired card date"
        expiredDateTextField.type = .expiredDate
    }


}

