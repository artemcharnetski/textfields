//
//  ContentViewSupportable.swift
//  TextFields
//
//  Created by Артём Чернецкий on 29/04/2019.
//  Copyright © 2019 Артём Чернецкий. All rights reserved.
//

import UIKit

protocol ContentViewSupportable {
    
    var contentView: UIView! { get }
    
}

extension ContentViewSupportable where Self: UIView {
    
    func initWithNib() {
        let nibName = String(describing: type(of: self))
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
