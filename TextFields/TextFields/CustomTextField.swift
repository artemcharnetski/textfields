//
//  OnlyNumbersTextField.swift
//  TextFields
//
//  Created by Артём Чернецкий on 29/04/2019.
//  Copyright © 2019 Артём Чернецкий. All rights reserved.
//

import Foundation
import UIKit

final class CustomTextField: UIView, ContentViewSupportable {
    
    enum TextFieldType {
        case numberAndPunctuationCharacters
        case maxFiveCharacters
        case expiredDate
        case allCharacters
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak private var textField: UITextField!
    @IBOutlet weak private var titleLabel: UILabel!
    
    var text: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
        }
    }
    var title: String? {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }
    var type: TextFieldType = .allCharacters
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        initWithNib()
        textField.delegate = self
    }
    
}

extension CustomTextField: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch type {
        case .numberAndPunctuationCharacters:
            var characterSet = CharacterSet.decimalDigits
            characterSet = characterSet.union(CharacterSet.punctuationCharacters).inverted
            return string.rangeOfCharacter(from: characterSet) == nil
        case .allCharacters:
            return true
        case .maxFiveCharacters:
            return textField.isEqual(range: range, string: string, to: 5)
        case .expiredDate:
            let compSepByCharInSet = string.components(separatedBy: CharacterSet.decimalDigits.inverted)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if string == numberFiltered {
                let isTextFieldChangeable = textField.isEqual(range: range, string: string, to: 4)
                guard let textFieldText = textField.text else {
                    return true
                }
                switch range.location {
                case 3:
                    textField.text = textFieldText.formatter(mask: "XX/XX")
                    break
                case 4:
                    textField.text = textFieldText.formatter(mask: "XXXX")
                    break
                default:
                    break
                }
                return isTextFieldChangeable
            } else {
                return false
            }
        }
    }
    
}
