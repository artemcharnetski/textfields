//
//  String.swift
//  TextFields
//
//  Created by Артём Чернецкий on 29/04/2019.
//  Copyright © 2019 Артём Чернецкий. All rights reserved.
//

import Foundation

extension String {
    
    func formatter(mask: String) -> String {
        let cleanString = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        var result = ""
        var index = cleanString.startIndex
        for character in mask where index < cleanString.endIndex {
            if character == "X" {
                result.append(cleanString[index])
                index = cleanString.index(after: index)
            } else {
                result.append(character)
            }
        }
        return result
    }
    
}
