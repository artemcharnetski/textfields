//
//  TextField.swift
//  TextFields
//
//  Created by Артём Чернецкий on 29/04/2019.
//  Copyright © 2019 Артём Чернецкий. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
 
    func isEqual(range: NSRange, string: String, to length: Int) -> Bool {
        guard let textFieldText = self.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= length
    }
    
}
